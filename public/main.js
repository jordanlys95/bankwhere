'use strict'

mapboxgl.accessToken = 'pk.eyJ1Ijoiam9yZGFubHlzOTUiLCJhIjoiY2thMjRoZjk5MDhkejNmbzV2NnQ3OGR2aCJ9.lKziTfam_bfJEQ43Eh3rig';
var banks = turf.featureCollection([]);
var map = init();
var geolocate = new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true
    },
    trackUserLocation: true
});

geolocate.on('geolocate', function (e) {
    var geoArray = [];
    var lon = e.coords.longitude;
    var lat = e.coords.latitude
    var position = [lon, lat];
    var userPoint = turf.point([lon, lat], { type: 'user' })
    geoArray.push(position);

    var nearest = turf.nearestPoint(userPoint, banks);
    console.log(position, nearest.properties.name);
    geoArray.push(turf.getCoords(nearest));
    var distance = turf.distance(userPoint, nearest, { units: 'kilometers' });
    var line = turf.lineString(geoArray, { name: 'nearest-bank', distance: distance.toFixed(2) + 'KM' });
    map.getSource('nearest-bank-line').setData(turf.featureCollection([line]));
    map.getSource('nearest-bank-label').setData(turf.featureCollection([line]));

});

function init() {
    var _map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [103.847, 1.339], // starting position [lng, lat]
        zoom: 10 // starting zoom
    });


    _map.on('style.load', function () {
        // add geolocation control
        _map.addControl(geolocate, 'bottom-left');

        // add source
        _map.addSource('banks', { type: 'geojson', data: banks });
        _map.addSource('nearest-bank-line', { type: 'geojson', data: turf.featureCollection([]) });
        _map.addSource('nearest-bank-label', {
            'type': 'geojson',
            'data': turf.featureCollection([])
        });

        fetchBanksLocations();

        // add layer
        _map.addLayer({
            'id': 'branch',
            'type': 'circle',
            'source': 'banks',
            paint: {
                'circle-color': '#0044a0',
                'circle-radius': 5,
                'circle-stroke-width': 2,
                'circle-stroke-color': '#fff'
            },
            'filter': ["any",
                ['==', 'type', 'Branch'],
                ['==', 'type', 'Privilege']
            ]
        });
        // _map.addLayer({
        //     'id': 'privilege',
        //     'type': 'circle',
        //     'source': 'banks',
        //     paint: {
        //         'circle-color': '#a98274',
        //         'circle-radius': 5,
        //         'circle-stroke-width': 2,
        //         'circle-stroke-color': '#fff'
        //     },
        //     'filter': ['==', 'type', 'Privilege']
        // });
        _map.addLayer({
            'id': 'nearest-bank-line',
            'type': 'line',
            'source': 'nearest-bank-line',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#ff6ec7',
                'line-width': 1
            }
        })

        _map.addLayer({
            'id': 'nearest-bank-label',
            'type': 'symbol',
            'source': 'nearest-bank-label',
            'layout': {
                'symbol-placement': 'line-center',
                // 'text-rotation-alignment': 'viewport',
                // 'text-keep-upright': true,
                'text-field': ['get', 'distance'],
            },
            'paint': {
                'text-translate': [0,24],
                'text-color': '#ff6ec7'

            }
        });




    })

    _map.on('click', 'branch', mapClick);
    //_map.on('click', 'privilege', mapClick);

    // Change the cursor to a pointer when the mouse is over the places layer.
    _map.on('mouseenter', 'places', function () {
        _map.getCanvas().style.cursor = 'pointer';
    });

    // Change it back to a pointer when it leaves.
    _map.on('mouseleave', 'places', function () {
        _map.getCanvas().style.cursor = '';
    });


    return _map;

}

function mapClick(e) {
    var coordinates = e.features[0].geometry.coordinates.slice();
    var { name, address, postalcode, OpHrMonFri, OpHrMonFriEarly, OpHrMonFriLate, OpHrMonFriFull, OpHrSat, OpHrSatEarly, OpHrSatLate, OpHrSatFull, OpHrSun } = e.features[0].properties;
    //OpHrMonFri	OpHrMonFriEarly	OpHrMonFriLate	OpHrMonFriFull	OpHrSat	OpHrSatEarly	OpHrSatLate	OpHrSatFull	OpHrSun
    var description = `
            <div>
                <strong>${name}</strong>
                <p>${address}, <br>${postalcode}</p>
                <p>
                    ${(OpHrMonFri) ? 'Mon - Fri: ' + OpHrMonFri + '<br>' : ''}

                    ${(OpHrSat) ? 'Sat: ' + OpHrSat + '<br>' : ''}

                    ${(OpHrSun) ? 'Sun: ' + OpHrSun : ''}
                </p>
                <p>
                    <a target="_blank" href="https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=${e.lngLat.lat},${e.lngLat.lng}">Street View</a> | 
                    <a target="_blank" href="https://waze.com/ul?ll=${e.lngLat.lat},${e.lngLat.lng}&z=10&navigate=yes">Waze</a> | 
                </p>
            </div>
        `;

    // Ensure that if the map is zoomed out such that multile
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(description)
        .addTo(map);
}

function fetchBanksLocations() {
    fetch('banks.geojson')
        .then(response => response.json())
        .then(({ features }) => {
            var filter = ['Branch', 'Privilege'];
            var filteredFeatures = features.filter(feature => (filter.indexOf(feature.properties.type) > -1));
            banks = turf.featureCollection(filteredFeatures);
            map.getSource('banks').setData(banks);
        });

}
